import 'package:flutter/material.dart';

class Settings extends StatefulWidget {
  const Settings({Key? key}) : super(key: key);

  @override
  State<Settings> createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: const Text("Settings"),
          centerTitle: true,
          //Removes the back button
          automaticallyImplyLeading: false),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              const Center(
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text(
                    "Settings",
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              ListView(
                // padding: const EdgeInsets.all(8),
                shrinkWrap: true,
                children: const <Widget>[
                  ListTile(
                    title: Text("General"),
                    subtitle: Text("General app settings"),
                    leading: Icon(Icons.settings_outlined),
                  ),
                  ListTile(
                    title: Text("Help & Feedback"),
                    subtitle: Text("Get back to us"),
                    leading: Icon(Icons.feedback_outlined),
                  ),
                  ListTile(
                    title: Text("Storage & Data Use"),
                    subtitle: Text("Change storage location"),
                    leading: Icon(Icons.sd_storage_outlined),
                  ),
                  ListTile(
                    title: Text("Delete Profile"),
                    subtitle: Text("Permanently delete your profile"),
                    leading: Icon(Icons.delete_forever_outlined),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
