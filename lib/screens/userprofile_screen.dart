import 'package:firebase_mtn/home.dart';
import 'package:flutter/material.dart';

class UserProfile extends StatelessWidget {
  const UserProfile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: const Text("User Profile"), //Removes the back button
          centerTitle: true,
          automaticallyImplyLeading: false),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            const Text(
              "Update your profile",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
            const TextField(
              decoration: InputDecoration(hintText: "Full Name"),
            ),
            const TextField(
              decoration: InputDecoration(hintText: "Date of Birth"),
            ),
            const TextField(
              decoration: InputDecoration(hintText: "Username"),
            ),
            const Padding(
              padding: EdgeInsets.all(8.0),
              child: ElevatedButton(onPressed: null, child: Text("Save")),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: ElevatedButton(
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => const Home()));
                  },
                  child: const Text("Cancel")),
            ),
          ],
        ),
      ),
    );
  }
}
