import 'package:flutter/material.dart';

class Chatroom extends StatefulWidget {
  const Chatroom({Key? key}) : super(key: key);

  @override
  State<Chatroom> createState() => _ChatroomState();
}

class _ChatroomState extends State<Chatroom> {
  //int _selectedItem = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Chatroom"),
        centerTitle: true,
      ),
      body: Column(
        children: const [
          Center(
            child: Padding(
              padding: EdgeInsets.all(8.0),
              child: Text(
                "Chatroom",
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
            ),
          ),
          Center(
              child: Padding(
            padding: EdgeInsets.all(9.0),
            child: Text("Chat using the below chat box."),
          )),
          Padding(
            padding: EdgeInsets.all(8.0),
            child: TextField(
                decoration: InputDecoration(hintText: "Type message...")),
          ),
          Padding(
            padding: EdgeInsets.all(8.0),
            child: ElevatedButton(onPressed: null, child: Text("Send")),
          ),
        ],
      ),
    );
  }
} // class


