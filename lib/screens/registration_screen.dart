import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_mtn/home.dart';
import 'package:firebase_mtn/screens/login_screen.dart';
import 'package:flutter/material.dart';

class Registration extends StatelessWidget {
  Registration({Key? key}) : super(key: key);

  //late BuildContext context;

  final TextEditingController controllerName = TextEditingController();
  final TextEditingController controllerUsername = TextEditingController();
  final TextEditingController controllerEmail = TextEditingController();
  final TextEditingController controllerPassword = TextEditingController();
  final TextEditingController controllerConfirm = TextEditingController();

  late bool registrationSuccess = false;

  @override
  Widget build(BuildContext context) {
    //this.context = context;

    return Scaffold(
      appBar: AppBar(
          title: const Text("Registration"),
          centerTitle: true,
          //Removes the back button
          automaticallyImplyLeading: false),
      body: Padding(
        padding: const EdgeInsets.all(9.0),
        child: Column(
          children: [
            const Center(
              child: Text(
                "Create Account",
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
            ),
            const Text("Welcome onboard!"),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                decoration: const InputDecoration(
                    border: OutlineInputBorder(), labelText: "Full Name"),
                controller: controllerName,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                decoration: const InputDecoration(
                    border: OutlineInputBorder(), labelText: "Username"),
                controller: controllerUsername,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                decoration: const InputDecoration(
                    border: OutlineInputBorder(), labelText: "Email"),
                controller: controllerEmail,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                obscureText: true,
                decoration: const InputDecoration(
                    border: OutlineInputBorder(), labelText: "Password"),
                controller: controllerPassword,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                obscureText: true,
                decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: "Confirm Password"),
                controller: controllerConfirm,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: ElevatedButton(
                  onPressed: () {
                    final name = controllerName.text.trim();
                    final username = controllerUsername.text.trim();
                    final email = controllerEmail.text.trim();
                    final password = controllerPassword.text.trim();
                    final confirm = controllerConfirm.text.trim();

                    if (name.isEmpty ||
                        username.isEmpty ||
                        email.isEmpty ||
                        password.isEmpty ||
                        confirm.isEmpty) {
                      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                          content: Text("Fields cannot be empty.")));
                    } else if (password != confirm) {
                      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                          content: Text("Passwords do no match.")));
                    } else {
                      createUserUsingPasswordEmail(email, password, context);

                      print(registrationSuccess);

                      if (registrationSuccess) {
                        createUser(
                            name, username, email, password, confirm, context);

                        ScaffoldMessenger.of(context)
                            .showSnackBar(const SnackBar(
                          content: Text("Registration successful."),
                          backgroundColor: Colors.green,
                        ));

                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: ((context) => const Home())));
                      }
                    }
                  },
                  child: const Text("Register")),
            ),
            Padding(
                padding: const EdgeInsets.all(8.0),
                child: InkWell(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Login()));
                  },
                  child: const Text("Already have an account? Login here."),
                )),
          ],
        ),
      ),
    );
  }

  Future createUser(String name, String username, String email, String password,
      String confirm, BuildContext context) async {
    // Reference to document
    final docUser = FirebaseFirestore.instance.collection("mtn_users").doc();

    //createUserUsingPasswordEmail(email, password, context);

    final json = {
      "id": docUser.id,
      "name": name,
      "username": username,
      "email": email,
    };

// Create document and write to Firebase
    //await docUser.set(json);
  } // close createUser()

  Future createUserUsingPasswordEmail(
      String email, String password, BuildContext context) async {
    try {
      final userCredential = await FirebaseAuth.instance
          .createUserWithEmailAndPassword(email: email, password: password);
      registrationSuccess = true;
    } on FirebaseException catch (e) {
      if (e.code == 'email-already-in-use') {
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
            content: Text("The account already exists for that email")));
        print('The account already exists for that email');
        registrationSuccess = false;
      } else if (e.code == 'weak-password') {
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
            content: Text("The password provided is too weak.")));
        print("The password provided is too weak.");
        registrationSuccess = false;
      }
    } catch (e) {
      late String error = e.toString();
      ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text("An unexpected error occured")));
      print(e);
    }
  } // close createUserUsingPasswordEmail()
}
