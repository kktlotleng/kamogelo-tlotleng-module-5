// File generated by FlutterFire CLI.
// ignore_for_file: lines_longer_than_80_chars, avoid_classes_with_only_static_members
import 'package:firebase_core/firebase_core.dart' show FirebaseOptions;
import 'package:flutter/foundation.dart'
    show defaultTargetPlatform, kIsWeb, TargetPlatform;

/// Default [FirebaseOptions] for use with your Firebase apps.
///
/// Example:
/// ```dart
/// import 'firebase_options.dart';
/// // ...
/// await Firebase.initializeApp(
///   options: DefaultFirebaseOptions.currentPlatform,
/// );
/// ```
class DefaultFirebaseOptions {
  static FirebaseOptions get currentPlatform {
    if (kIsWeb) {
      return web;
    }
    switch (defaultTargetPlatform) {
      case TargetPlatform.android:
        return android;
      case TargetPlatform.iOS:
        return ios;
      case TargetPlatform.macOS:
        return macos;
      case TargetPlatform.windows:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for windows - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      case TargetPlatform.linux:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for linux - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      default:
        throw UnsupportedError(
          'DefaultFirebaseOptions are not supported for this platform.',
        );
    }
  }

  static const FirebaseOptions web = FirebaseOptions(
    apiKey: 'AIzaSyBbj7ry5pmg8co0G36wr7BDJbGpa4zz3so',
    appId: '1:471945878474:web:ab00c8f242fee9d35a8d33',
    messagingSenderId: '471945878474',
    projectId: 'm5mtn-64bff',
    authDomain: 'm5mtn-64bff.firebaseapp.com',
    storageBucket: 'm5mtn-64bff.appspot.com',
  );

  static const FirebaseOptions android = FirebaseOptions(
    apiKey: 'AIzaSyD83Hjtn3HxStatZXXtly_j8al1Rr3Q6cw',
    appId: '1:471945878474:android:475f1409cb8988845a8d33',
    messagingSenderId: '471945878474',
    projectId: 'm5mtn-64bff',
    storageBucket: 'm5mtn-64bff.appspot.com',
  );

  static const FirebaseOptions ios = FirebaseOptions(
    apiKey: 'AIzaSyADozVqYdc64MJ1KY5PZgoW6v_wh9nMz0A',
    appId: '1:471945878474:ios:90ba5a25f466f5f25a8d33',
    messagingSenderId: '471945878474',
    projectId: 'm5mtn-64bff',
    storageBucket: 'm5mtn-64bff.appspot.com',
    iosClientId: '471945878474-sp672p00k1b71e0849jhm3jbauf2pcid.apps.googleusercontent.com',
    iosBundleId: 'com.example.firebaseMtn',
  );

  static const FirebaseOptions macos = FirebaseOptions(
    apiKey: 'AIzaSyADozVqYdc64MJ1KY5PZgoW6v_wh9nMz0A',
    appId: '1:471945878474:ios:90ba5a25f466f5f25a8d33',
    messagingSenderId: '471945878474',
    projectId: 'm5mtn-64bff',
    storageBucket: 'm5mtn-64bff.appspot.com',
    iosClientId: '471945878474-sp672p00k1b71e0849jhm3jbauf2pcid.apps.googleusercontent.com',
    iosBundleId: 'com.example.firebaseMtn',
  );
}
